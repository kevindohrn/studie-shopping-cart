from __future__ import annotations



def get_dynamic_table(orders: list[list[str | int]]) -> list[list[str | int]]:
    """
    orders besteht aus einer Liste aus Listen. Eine Liste besteht jeweils aus einer order_item_id (int), einer order_id (int) und
    dem product_name (str).

    :param orders: list[list[str | int]] Rohe Daten, aus welchen eine Statistik erstellt wird
    :return: das 2d Array mit der Statistik mit den product_name als Spaltennamen und der order_id als Reihennamen
    """
    if not orders:
        return orders

    all_product_names = []
    all_orders = {}

    for order_item_id, order_id, product_name in orders:
        if product_name not in all_product_names:
            all_product_names.append(product_name)

        if order_id not in all_orders:
            all_orders[order_id] = {}

        if product_name in all_orders[order_id]:
            all_orders[order_id][product_name] += 1
        else:
            all_orders[order_id][product_name] = 1

    all_product_names.sort()
    all_column_headers = ["Order ID"] + all_product_names + ["Total"]

    dynamic_table = [all_column_headers]

    for row_order_id in sorted(all_orders):
        row = [row_order_id]
        row_total = 0
        for product_name in all_product_names:
            count = all_orders[row_order_id].get(product_name, 0)
            row.append(count)
            row_total += count
        row.append(row_total)
        dynamic_table.append(row)

    total_row = ["Total"]
    for i in range(1, len(all_column_headers) - 1):  # Exclude the "Total" column from summing
        column_sum = sum(row[i] for row in dynamic_table[1:])
        total_row.append(column_sum)
    
    # Adding the total of the "Total" column
    total_row.append(sum(total_row[1:]))

    dynamic_table.append(total_row)

    return dynamic_table if len(dynamic_table) > 1 else []


def calculate_row_totals(dynamic_table: list[list[str | int]]) -> list[list[str | int]]:
    return dynamic_table


def calculate_column_totals(dynamic_table: list[list[str | int]]) -> list[list[str | int]]:
    pass
