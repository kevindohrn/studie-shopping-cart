from __future__ import annotations


def get_prices_from_cart(products: list[dict[str, int | str]]) -> list[int]:
    prices: list[int] = []

    for product in products:
        prices.append(product["price"])

    return prices


def calculate_product_sum_with_discount_util(products: list[dict[str, int | str]]) -> int:
    prices = get_prices_from_cart(products)

    # TODO: Implementieren eines speziellen Rabattes, der die Summe des neuen Warenkorbs berechnet
    # Berechnung des speziellen Rabattes
    total_price = 0
    for i in range(len(prices)):
        aktueller_preis = prices[i]
        neuer_preis = aktueller_preis
        for j in range(i + 1, len(prices)):
            if prices[j] <= aktueller_preis:
                neuer_preis = aktueller_preis - prices[j]
                break
        total_price += neuer_preis
    
    return total_price
